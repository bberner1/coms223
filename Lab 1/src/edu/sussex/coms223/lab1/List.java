package edu.sussex.coms223.lab1;

public interface List<E>
{
	boolean add(E e);
	E get(int i);
	boolean remove(E e);
	int size();
	void clear();
}
