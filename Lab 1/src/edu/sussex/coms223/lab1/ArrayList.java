package edu.sussex.coms223.lab1;

public class ArrayList<E> implements List<E>
{
	private Object[] data=new Object[0];
	public boolean add(E e)
	{
		if(e==null)
		{
			throw new IllegalArgumentException("No null values allowed");
		}
		else
		{
			Object[] ndata=new Object[data.length+1];
			for(int i=0; i<data.length; i++)
			ndata[i]=data[i];
			ndata[data.length]=e;
			data=ndata;
			return true;
		}
		
	}
	public E get(int i)
	{
		if(i<0||i>=data.length)
		throw new IllegalArgumentException("Illegal index");
		E e= (E)data[i];
		return e;	
	}
	public boolean remove(E e) 
	{
		if(e!=null&&data.length!=0)
		{
			Object[] ndata=new Object[data.length-1];
			boolean removed=false;
			int j=0;
			for(int i=0; i<data.length; i++)
			{
				if((data[i]!=null&&data[i].equals(e))||!removed)
				{
					removed=true;
					j--;
				}
				else if(j<ndata.length)
					ndata[j]=data[i];
				j++;
			}
			data=ndata;
			return removed;
		}
		return false;
	}
	public int size() 
	{
		return data.length;
	}
	public void clear() 
	{
		data=new Object[0];
	}
}